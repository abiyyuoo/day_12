<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('latihan.register');
    }
    public function welcome(Request $request){
        // dd($request->all());
        $first_name = $request['fname'];
        $last_name = $request['lname'];
        $male = $request['gender1'];
        $female = $request['gender2'];
        $other_gander = $request['gender3'];
        $negara = $request['negara'];
        $bind = $request['l1'];
        $bing = $request['l2'];
        $other_bhs = $request['l3'];
        $bio = $request['bio'];
        return view('latihan.welcome', compact(
            'first_name', 'last_name',
            'male','female','other_gander'
            ,'negara','bind','bing','other_bhs','bio'));
    }
}
