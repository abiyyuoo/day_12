<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method_field="PUT">
        @csrf
        <p>First Name</p>
        <input type="text" name="fname">
        <p>Last Name</p>
        <input type="text" name="lname">
        <p>Gender</p>
        <input type="radio" name="gender1" id="1">Male
        <input type="radio" name="gender2" id="2">Female
        <input type="radio" name="gender3" id="3">Other
        <p>Nationality:</p>
        <select name="negara" id="negara">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapore">Singapore</option>
        </select>
        <p>Language Spoken</p>
        <input type="checkbox" name="l1" id="indo">Bahasa Indonesia
        <input type="checkbox" name="l2" id="ing">Bahasa Inggris
        <input type="checkbox" name="l3" id="oth">Other
        <p>Bio:</p>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
        <input type="submit" value="Submit">
    </form>
    
</body>
</html>